import { Router } from 'express';
import UserController from '../controllers/users.controller';
import BeerController from '../controllers/beer.controller';
import CommentController from '../controllers/comments.controller';
import User from "../models/User";
import Post from "../models/Post";
import Beer from "../models/Beer";

const router = Router();

//ROUTER

//GET, POST, PUT, DELETE, 

router.get('/test', function(req, res){
    res.send("test");
});
//Beers routes
router.get('/beers', BeerController.list);
router.get('/topbeers', BeerController.listTopBeers);
router.post('/beers', BeerController.create);
router.get('/beers/:id', BeerController.details);
router.get('/beers/:id', BeerController.detailsTopBeers);
router.delete('/beers/:id', BeerController.delete);
router.put('/beers/:id', BeerController.update);

//Users routes
router.get('/users', UserController.list);
router.post('/users', UserController.create);
router.post('/users/authenticate', UserController.auth);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', UserController.delete);
router.put('/users/:id', UserController.update);



//Comments routes
router.get('/comments', CommentController.list);
router.post('/comments', CommentController.create);
router.get('/comments/:id', CommentController.details);
router.delete('/comments/:id', CommentController.delete);
router.put('/comments/:id', CommentController.update);

export default router;

//Postman , MongoDB Community Edition - (Robot3T)