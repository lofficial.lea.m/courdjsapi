import mongoose from 'mongoose';

//Création du schema d'associations des bières et de leurs notes
const scoreSchema = new mongoose.Schema({
    score: {
        type: Number,
        required: true
    },
    beerId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Beer'
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }

});

const Post = mongoose.model('Post', scoreSchema);
export default Post;