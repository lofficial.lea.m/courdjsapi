import mongoose from 'mongoose';

//Création du schema des bieres
const beerSchema = new mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    // "Slogan"
    tagline: {
        type: String,
        required: false
    },
    description: {
        type: String,
        required: false
    },
    // Alcool by Volume
    ABV: {
        type: Number,
        required: false
    },
    // Couleur de la bière, clair = 0 # foncé = 100
    EBC: {
        type: Number,
        required: false
    },
    // url de l'image
    image: {
        type: String,
        required: false
    }
});

const Beer = mongoose.model('Beer', beerSchema);
export default Beer;