import Beer from "../models/Beer";

class BeerController {

    /**
     * Create Article into Database
     * @param {Request} req
     * @param {Response} res
     */
    static async create(req, res) {
        let status = 200;
        let body = {};

        try {

            let beer = await Beer.create({
                name: req.body.name,
                tagline: req.body.tagline,
                description: req.body.description,
                ABV: req.body.ABV,
                EBC: req.body.EBC,
                image: req.body.image
            });
            body = {
                beer,
                'message': 'Beer created !'
            };

        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let beer = await Beer.find().populate('beerID');

            body = {
                beer,
                'message': 'Beers list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let beer = await Beer.findByID(id).populate('beerID');

            body = {
                beer,
                'message': 'Beers details'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            await Beer.remove({_id: req.params.id});
            body = {
                'message': 'Beer delete'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // methode 1
            let beer = await Beer.findById(req.params.id);
            Object.assign(beer, req.body);
            await beer.save();

            body = {
                beer,
                'message': 'Beer update'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async listTopBeers(req, res) {
        let status = 200;
        let body = {};

        try {
            let beer = await Beer.find().populate('beerID').limit(10);

            body = {
                beer,
                'message': 'Top 10 beers list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async detailsTopBeers(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let beer = await Beer.findByID(id).populate('beerID').limit(5);

            body = {
                beer,
                'message': 'Top 5 Beers details'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
}

export default BeerController;