import User from "../models/User";
import jwt from "jsonwebtoken";

class UserController{

    /**
     * Create User into Database
     * @param {Request} req
     * @param {Response} res
     */
    static async create(req, res){
        let status = 200;
        let body = {};

        try {

            let user = await User.create({
                firstname: req.body.firstname,
                lastname: req.body.lastname,
                email: req.body.email,
                password: req.body.password
            });

            body = {
                user,
                'message': 'User created'
            };

        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }


    static async list(req, res){
        let status = 200;
        let body = {};

        try {
            let user = await User.find();

            //.find()
            //.findByID(id)
            // .findOne({email: 'testest'})
            body = {
                user,
                'message': 'Users list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let user = await User.findByID(id);

            body = {
                user,
                'message': 'Users details'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            await User.remove({_id: req.params.id});
            body = {
                'message': 'Users delete'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // methode 1
            let user = await User.findById(req.params.id);
            delete req.body.password;
            Object.assign(user, req.body);
            await user.save();

            // méthode 2
            /*await User.findByIDAndUpdate({_id: req.params.id}, {$set: req.body});
           let user = await User.findByIdAndUpdate(req.params.id, req.body, {new: true});
        */
            // métohode 3
           /* await  user.update({
                firstname: req.body.firstname
            });
            */
            body = {
                user,
                'message': 'Users update'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
    static async auth(req, res) {
        let status = 200;
        let body = {};

        try {
            //checker si le user a le bon mdp et email
            let user = await User.findOne({email: req.body.email});
            if( user && user.password === req.body.password) {
                //génere le jwt
                let token = jwt.sign({sub: user._id}, "mon secret");

                body = {
                    user,
                    token,
                    'message': 'Users authenticated'
                };
            }else{
                status = 401;
                body = {
                    'message' : 'Error email or password'
                }
            }
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }
}
export default UserController;